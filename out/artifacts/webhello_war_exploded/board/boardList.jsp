<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="myboard.entity.Board" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title></title>
  </head>
  <body>
   <%= request.getAttribute("result") %>

   <table>
       <tr>
           <td>title</td>
           <td>writer</td>
           <td>pw</td>
           <td>modify</td>
           <td>delete</td>
           <td>detail</td>
       </tr>
  <%

      List<Board> boards = (List<Board>) request.getAttribute("boards");

      for (Board board : boards) {
  %><tr><%
           out.write("<td>"+board.getTitle()+"</td>\n");
           out.write("<td>"+board.getWriter()+"</td>\n");
           out.write("<td>"+board.getPw()+"</td>\n");
           out.write("<td> <form method='POST' action='/board/putItem'> <input type='hidden' name='id' value='" + board.getId() + "'/> <input type='submit' value='modify'/> </form> </td>\n");
           out.write("<td> <form method='POST' action='/board/deleteItem'> <input type='hidden' name='id' value='" + board.getId() + "'/> <input type='submit' value='delete'/> </form> </td>\n");
           out.write("<td> <form method='GET' action='/board/getItem'> <input type='hidden' name='id' value='" + board.getId() + "'/> <input type='submit' value='detail'/> </form> </td>\n");
  %></tr><%
       }

  %>
   </table>

   <%
       Boolean displayLogin = (Boolean) request.getAttribute("displayLogin");
       String userId = (String) request.getAttribute("id");
   %>
   <div class="login">

    <form method="POST" action="/login">
        <%
            out.write("ID : <input type='text' name='id' value='" + userId + "' />");
        %>
        PASSWORD: <input type="password" name="password" />
        <input type="submit" value="login" />
    </form>
       <form method="POST" action="/logout">
        <input type="submit" value="logout" />
       </form>
   </div>


   <div class="addItem">
   <form method="POST" action="/board/postItem">
       <!-- title, writer, contents, password -->
       title <input type="text" name="title" />
       writer <input type="text" name="writer" />
       contents <input type="text" name="contents" />
       ps <input type="password" name="password" />
       <input type="submit" value="Save">
   </form>
   </div>


   <input type="button" value="등록" onclick="goSaveForm()" />
  </body>
</html>