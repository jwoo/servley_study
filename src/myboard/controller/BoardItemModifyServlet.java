package myboard.controller;

import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jays
 * Date: 3/6/13
 * Time: 12:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class BoardItemModifyServlet extends HttpServlet {
        BoardRepository boardRepository = BoardMemoryRepository.getInstance();

        //model에서 지정된 Item list 조회
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            List<Board> boards = boardRepository.getBoards();

            int board_id = Integer.parseInt(request.getParameter("id"));

            Board detailBoard = boardRepository.getBoard(board_id);

            //2. request에 데이터 셋팅
            request.setAttribute("board", detailBoard);

            RequestDispatcher view = request.getRequestDispatcher("boardPutItem.jsp");

            view.forward(request, response);
        }
}
