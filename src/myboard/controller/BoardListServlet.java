package myboard.controller;

import hello.ResultModel;
import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * User: HolyEyE
 * Date: 13. 2. 22. Time: 오후 4:37
 */
public class BoardListServlet extends HttpServlet{

    BoardRepository boardRepository = BoardMemoryRepository.getInstance();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //1. model에서 데이터 조회
        List<Board> boards = boardRepository.getBoards();
        String id = null;

        for (Board board : boards) {
            System.out.println("board = " + board);
        }

        Cookie[] cookies = request.getCookies();

        for ( int i = 0; i < cookies.length; i++) {
            Cookie cookie = cookies[i];
            System.out.println("COO " + cookie);
            if (cookie.getName().equals("id")) {
                id = cookie.getValue();
            }

        }

        HttpSession session = request.getSession();

        if (session != null) {
            request.setAttribute("displayLogin", true);
        } else {
            request.setAttribute("displayLogin", false);
        }

        //2. request에 데이터 셋팅
        request.setAttribute("boards",boards);
        request.setAttribute("id", id);

        //3. jsp찾아서 이동
        RequestDispatcher view = request.getRequestDispatcher("/board/boardList.jsp");

        response.setContentType("text/html");
        view.forward(request, response);
    }
}
