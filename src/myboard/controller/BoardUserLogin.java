package myboard.controller;

import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.net.CookiePolicy;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jays
 * Date: 3/11/13
 * Time: 7:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class BoardUserLogin extends HttpServlet {
    BoardRepository boardRepository = BoardMemoryRepository.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String pw = request.getParameter("password");

        Cookie cookie = new Cookie("id", id);
        cookie.setMaxAge(30*24*60*60);
        response.addCookie(cookie);

        HttpSession session = request.getSession();
        session.setAttribute("id", id);
        session.setAttribute("pw", pw);

        List<Board> boards = boardRepository.getBoards();

        for (Board board : boards) {
            System.out.println("board = " + board);
        }


        request.setAttribute("session", session);
        request.setAttribute("boards",boards);

        RequestDispatcher view = request.getRequestDispatcher("/board/boardList.jsp");
        response.setContentType("text/html");

        view.forward(request, response);

    }

}
