package myboard.controller;


import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jays
 * Date: 3/11/13
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class BoardUserLogout extends HttpServlet {
    BoardRepository boardRepository = BoardMemoryRepository.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        session.invalidate();

        List<Board> boards = boardRepository.getBoards();

        for (Board board : boards) {
            System.out.println("board = " + board);
        }

        request.setAttribute("boards", boards);
        RequestDispatcher view = request.getRequestDispatcher("/board/boardList.jsp");

        view.forward(request, response);
    }
}
