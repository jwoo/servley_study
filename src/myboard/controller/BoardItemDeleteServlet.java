package myboard.controller;

import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.http.HttpServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jays
 * Date: 3/5/13
 * Time: 8:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class BoardItemDeleteServlet extends HttpServlet {
    BoardRepository boardRepository = BoardMemoryRepository.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Board> boards = boardRepository.getBoards();

        int board_id = Integer.parseInt(request.getParameter("id"));


        boardRepository.deleteBoard(board_id);

        //2. request에 데이터 셋팅
        request.setAttribute("boards",boards);


        RequestDispatcher view = request.getRequestDispatcher("boardList.jsp");

        view.forward(request, response);
    }
}
