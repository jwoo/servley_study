package myboard.controller;

import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jays
 * Date: 3/6/13
 * Time: 1:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class BoardItemModifySetServlet extends HttpServlet {
    BoardRepository boardRepository = BoardMemoryRepository.getInstance();

    //model에서 지정된 Item list 조회
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Board> boards = boardRepository.getBoards();

        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String writer = request.getParameter("writer");
        String pw = request.getParameter("password");
        String content = request.getParameter("contents");

        Board modifyBoard = boards.get(id);
        modifyBoard.setTitle(title);
        modifyBoard.setWriter(writer);
        modifyBoard.setContent(content);
        modifyBoard.setPw(pw);


        request.setAttribute("boards",boards);
        System.out.println("RRR" + boards);
        RequestDispatcher view = request.getRequestDispatcher("boardList.jsp");

        view.forward(request, response);
    }
}
