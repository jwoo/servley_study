package myboard.controller;

import myboard.entity.Board;
import myboard.repository.BoardMemoryRepository;
import myboard.repository.BoardRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: jays
 * Date: 3/4/13
 * Time: 8:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class BoardItemServlet extends HttpServlet{
    BoardRepository boardRepository = BoardMemoryRepository.getInstance();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = 0;
        String title = request.getParameter("title");
        String writer = request.getParameter("writer");
        String pw = request.getParameter("password");
        String content = request.getParameter("contents");

        Board newBoard = new Board(id, title, writer, pw, content);

        boardRepository.addBoard(newBoard);


        List<Board> boards = boardRepository.getBoards();

        for (Board board : boards) {
            System.out.println("board = " + board);
        }

        request.setAttribute("boards",boards);

        RequestDispatcher view = request.getRequestDispatcher("boardList.jsp");

        view.forward(request, response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Board> boards = boardRepository.getBoards();

        int board_id = Integer.parseInt(request.getParameter("id"));

        //Id로 갖고 온 값으로 값 참음.

        RequestDispatcher view = request.getRequestDispatcher("boardPostItem.jsp");

        view.forward(request, response);
    }

   }
